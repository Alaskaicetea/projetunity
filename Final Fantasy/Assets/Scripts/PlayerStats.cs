﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {

	public int currentLvl;
	public int currentExp;

	public int[] toLvlUp;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (currentExp >= toLvlUp [currentLvl]) {
			currentExp++;
		}
	}

	public void AddExp(int expToAdd){
		currentExp += expToAdd;
	}
}

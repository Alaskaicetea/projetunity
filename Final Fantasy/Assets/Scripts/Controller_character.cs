﻿using UnityEngine;
using System.Collections;

public class Controller_character : MonoBehaviour {
	//Variables
	public float speed = 6.0F;
	private float currentMoveSpeed;
	public float diagonalMoveModifier;

	public float jumpSpeed = 8.0F; 
	public float gravity = 20.0F;
	private Vector3 moveDirection = Vector3.zero;
	
	private Animator anim;
	private Rigidbody2D myRigidBody;
	private bool playerMoving;
	public Vector2 lastMove;

	private static bool playerExists;
	void Start(){
		anim = GetComponent<Animator> ();
		myRigidBody = GetComponent<Rigidbody2D> ();
	
		if(!playerExists){
			playerExists=true;
			DontDestroyOnLoad (transform.gameObject);
		}else{
			Destroy(gameObject);
		}

	}

	void Update() {
		move_Character ();
	}
	
	void move_Character(){
		playerMoving = false;

		if(Input.GetAxisRaw("Horizontal") > 0.5f || Input.GetAxisRaw("Horizontal") < -0.5f){
			playerMoving = true;
			//transform.Translate(new Vector3(Input.GetAxisRaw("Horizontal")*speed*Time.deltaTime,0f,0f));
			myRigidBody.velocity = new Vector2(Input.GetAxisRaw("Horizontal")*currentMoveSpeed, myRigidBody.velocity.y );
			lastMove = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
		}

		if(Input.GetAxisRaw("Vertical") > 0.5f || Input.GetAxisRaw("Vertical") < -0.5f){
			playerMoving = true;
			//transform.Translate(new Vector3(0f,Input.GetAxisRaw("Vertical")*speed*Time.deltaTime,0f));
			myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, Input.GetAxisRaw("Vertical")*currentMoveSpeed );
			lastMove = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
		}

		if (Input.GetAxisRaw ("Horizontal") < 0.5f && Input.GetAxisRaw ("Horizontal") > -0.5f) {
			myRigidBody.velocity = new Vector2(0f, myRigidBody.velocity.y);
		}

		if (Input.GetAxisRaw ("Vertical") < 0.5f && Input.GetAxisRaw ("Vertical") > -0.5f) {
			myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, 0f);
		}

		if (Mathf.Abs (Input.GetAxisRaw ("Horizontal")) > 0.5f && Mathf.Abs (Input.GetAxisRaw ("Vertical")) > 0.5f) {
			currentMoveSpeed = speed * diagonalMoveModifier;
		} else {
			currentMoveSpeed = speed;
		}
		anim.SetFloat("MoveX",Input.GetAxis ("Horizontal"));
		anim.SetFloat("MoveY",Input.GetAxis ("Vertical"));
		anim.SetBool ("PlayerMoving", playerMoving);
		anim.SetFloat("LastMoveX",lastMove.x);
		anim.SetFloat ("LastMoveY", lastMove.y);
	}
}

﻿using UnityEngine;
using System.Collections;

public class PlayerStartPoint : MonoBehaviour {

	private Controller_character thePlayer;
	private Camera_Controller theCamera;

	public Vector2 startDirection;

	// Use this for initialization
	void Start () {
		thePlayer = FindObjectOfType<Controller_character> ();
		thePlayer.transform.position = transform.position;
		thePlayer.lastMove = startDirection;
		theCamera = FindObjectOfType<Camera_Controller> ();
		theCamera.transform.position = new Vector3 (transform.position.x, transform.position.y, theCamera.transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class BaseClass {

	public string theName;
	public float baseHp;
	public float currentHp;
	
	public float baseMp;
	public float currentMp;
	
	public float baseATK;
	public float curATK;
	public float baseDEF;
	public float curDEF;

}

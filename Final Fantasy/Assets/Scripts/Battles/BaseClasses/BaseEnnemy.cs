﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class BaseEnnemy: BaseClass  {



	public enum Type{
		FIRE,
		WATER,
		EARTH,
		VEG,
		ELECTRIC
	}

	public enum Rarity{
		COMMON,
		UNCOMMON,
		RARE,
		SUPERARE
	}

	public Type ennemyType;
	public Rarity rarity;
	

}

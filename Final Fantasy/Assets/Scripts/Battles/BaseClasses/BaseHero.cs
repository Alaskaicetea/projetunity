﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class BaseHero: BaseClass  {

	public int stamina;
	public int intellect;
	public int dexterity;
	public int agility;
}

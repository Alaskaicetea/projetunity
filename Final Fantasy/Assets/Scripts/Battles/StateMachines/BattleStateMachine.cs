﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BattleStateMachine : MonoBehaviour {

	public enum PerformAction{
		WAIT,
		TAKEACTION,
		PERFORMACTION
	}


	public PerformAction battleState;

	public List<HandleTurns> performList = new List<HandleTurns>();
	public List<GameObject> HeroesInGame = new List<GameObject>();
	public List<GameObject> EnnemysInGame= new List<GameObject>();

	public enum HeroGUI{
		ACTIVATE,
		WAITING,
		INPUT1,
		INPUT2,
		DONE
	}

	public HeroGUI HeroInput;

	public List<GameObject> HeroToManage = new List<GameObject>();
	private HandleTurns HeroChoice;

	public GameObject EnemyButton;
	public Transform Spacer;

	public GameObject AttackPanel;
	public GameObject EnemySelectPanel;


	// Use this for initialization
	void Start () {
		battleState = PerformAction.WAIT;
		HeroesInGame.AddRange(GameObject.FindGameObjectsWithTag("Hero"));
		EnnemysInGame.AddRange(GameObject.FindGameObjectsWithTag("Ennemy"));
		HeroInput = HeroGUI.ACTIVATE;
		AttackPanel.SetActive (false);
		EnemySelectPanel.SetActive (false);
		EnemyButtons ();
	}
	
	// Update is called once per frame
	void Update () {
		switch(battleState){
			case(PerformAction.WAIT):
				if(performList.Count > 0){
					battleState = PerformAction.TAKEACTION;
				}
			break;
			case(PerformAction.TAKEACTION):
				GameObject performer = GameObject.Find(performList[0].attacker);
		
				if(performList[0].Type == "Ennemy"){

					EnnemyStateMachine ESM = performer.GetComponent<EnnemyStateMachine>();
					ESM.heroToAttack = performList[0].attackerTarget;
					ESM.currentState = EnnemyStateMachine.TurnState.ACTION;
				}
				if(performList[0].Type == "Hero"){
					HeroStateMachine HSM = performer.GetComponent<HeroStateMachine>();
					HSM.EnnemyToAttack = performList[0].attackerTarget;	
					HSM.currentState = HeroStateMachine.TurnState.ACTION;
				}
				battleState = PerformAction.PERFORMACTION;
			break;
			case(PerformAction.PERFORMACTION):
			
			break;
		}

		switch (HeroInput) {
			case(HeroGUI.ACTIVATE):

				if(HeroToManage.Count > 0){

					HeroToManage[0].transform.FindChild("Selector").gameObject.SetActive(true);
					HeroChoice = new HandleTurns();
					AttackPanel.SetActive(true);
					HeroInput = HeroGUI.WAITING;
				}
			break;
			case(HeroGUI.WAITING):
			
			break;
			case(HeroGUI.DONE):
				HeroInputDone();
			break;
			case(HeroGUI.INPUT1):
			
			break;
		}
	}

	public void CollectActions(HandleTurns input){
		performList.Add (input);
	}

	void EnemyButtons(){
		foreach(GameObject enemy in EnnemysInGame){
			GameObject newButton = Instantiate(EnemyButton) as GameObject;
			EnemySelectButton button = newButton.GetComponent<EnemySelectButton>();

			EnnemyStateMachine cur_enemy = enemy.GetComponent<EnnemyStateMachine>();

			Text buttonText = newButton.transform.FindChild("Text").gameObject.GetComponent<Text>();
			buttonText.text = cur_enemy.ennemy.theName;
		
			button.EnemyPrefab = enemy;

			newButton.transform.SetParent(Spacer, false);
		}
	}

	public void Input1(){ //Attack Button
		HeroChoice.attacker = HeroToManage [0].name;
		HeroChoice.attackerGO = HeroToManage [0];
		HeroChoice.Type = "Hero";

		AttackPanel.SetActive (false);
		EnemySelectPanel.SetActive (true); //Affichage choix ennemy
	}

	public void Input2(GameObject choosenEnemy){ //Enemy selection
		HeroChoice.attackerTarget = choosenEnemy;
		HeroInput = HeroGUI.DONE;
	}

	void HeroInputDone(){
		performList.Add (HeroChoice);
		EnemySelectPanel.SetActive (false);
		HeroToManage [0].transform.FindChild("Selector").gameObject.SetActive(false);
		HeroToManage.RemoveAt (0);
		HeroInput = HeroGUI.ACTIVATE;
	}
}

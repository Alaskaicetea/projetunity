﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnnemyStateMachine : MonoBehaviour {

	private BattleStateMachine BSM;
	public BaseEnnemy ennemy;

	public enum TurnState
	{
		PROCESSING,
		CHOOSEACTION,
		WAITING,
		ACTION,
		DEAD
	}
	
	public TurnState currentState;
	//For the progress bar
	private float cur_cooldown=0f;
	private float max_cooldown=15f;
	public Image progressBar;

	//This gameObject
	private Vector3 startPosition;
	public GameObject heroToAttack;
	private float animSpeed = 5f;
	public GameObject Selector;
	//TimeforAction stuff
	private bool actionStarted = false;
	// Use this for initialization
	void Start () {
		currentState = TurnState.PROCESSING;
		Selector.SetActive (false);
		BSM = GameObject.Find("BattleManager").GetComponent<BattleStateMachine>();
		startPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		switch (currentState) {
			case(TurnState.PROCESSING):
				UpdateProgressBar();
			break;

			case(TurnState.CHOOSEACTION):
				ChooseAction();
				currentState=TurnState.WAITING;
			break;

			case(TurnState.ACTION):
			StartCoroutine(TimeForAction());
			break;
			case(TurnState.DEAD):
			
			break;
		}
	}

	void UpdateProgressBar(){
		cur_cooldown = cur_cooldown + Time.deltaTime;
		if(cur_cooldown >= max_cooldown){
			currentState=TurnState.CHOOSEACTION;
		}
	}

	void ChooseAction()
	{
		HandleTurns myAttack = new HandleTurns ();
		myAttack.attacker = ennemy.theName;
		myAttack.attackerGO = this.gameObject;
		myAttack.Type = "Ennemy";
		myAttack.attackerTarget = BSM.HeroesInGame [Random.Range (0, BSM.HeroesInGame.Count)];//Choix random de la cible
		BSM.CollectActions (myAttack);
	}

	private IEnumerator TimeForAction(){
		if(actionStarted){
			yield break;
		}
		actionStarted = true;

		//Animate ennemy
		Vector3 heroPos = new Vector3(heroToAttack.transform.position.x -1.5f, heroToAttack.transform.position.y, heroToAttack.transform.position.z);
		while(MoveTowardEnnemy(heroPos)){yield return null;}
		//wait
		yield return new WaitForSeconds (0.5f);

		//do damage


		//Animate back to position
		Vector3 firstPos = startPosition;
		while(MoveTowardStart(firstPos)){yield return null;}

		//remove performer from bsm list
		BSM.performList.RemoveAt (0);

		//reset BSM-> wait
		BSM.battleState = BattleStateMachine.PerformAction.WAIT;
		//End the coroutine
		actionStarted = false;
		//reset ennem
		cur_cooldown = 0f;
		currentState = TurnState.PROCESSING;
	}

	private bool MoveTowardEnnemy(Vector3 target){
		return target != (transform.position = Vector3.MoveTowards (transform.position, target, animSpeed * Time.deltaTime));
	}

	private bool MoveTowardStart(Vector3 target){
		return target != (transform.position = Vector3.MoveTowards (transform.position, target, animSpeed * Time.deltaTime));
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HeroStateMachine : MonoBehaviour {

	private BattleStateMachine BSM;
	public BaseHero hero;

	public enum TurnState
	{
		PROCESSING,
		ADDTOLIST,
		WAITING,
		SELECTION,
		ACTION,
		DEAD
	}

	public TurnState currentState;
	//For the progress bar
	private float cur_cooldown=0f;
	private float max_cooldown=5f;
	public Image progressBar;
	public GameObject Selector;
	//IeNumerator
	public GameObject EnnemyToAttack;
	private bool actionStarted = false;
	private Vector3 startPosition;
	private float animSpeed=5f;

	// Use this for initialization
	void Start () {
		startPosition = transform.position;
		cur_cooldown = Random.Range (0, 1.5f);
		Selector.SetActive (false);
		BSM = GameObject.Find ("BattleManager").GetComponent<BattleStateMachine> ();
		currentState = TurnState.PROCESSING;
	}
	
	// Update is called once per frame
	void Update () {
		switch (currentState) {
			case(TurnState.PROCESSING):
				UpdateProgressBar();
			break;
			case(TurnState.ADDTOLIST):
				BSM.HeroToManage.Add(this.gameObject);
				currentState= TurnState.WAITING;
			break;
			case(TurnState.WAITING):
				//Idle
			break;
			case(TurnState.SELECTION):
			
			break;
			case(TurnState.ACTION):
				StartCoroutine(TimeForAction());
			break;
			case(TurnState.DEAD):
			
			break;
		}
	}

	void UpdateProgressBar(){
		cur_cooldown = cur_cooldown + Time.deltaTime;
		float calc_cooldown = cur_cooldown / max_cooldown;
		progressBar.transform.localScale = new Vector3 (Mathf.Clamp (cur_cooldown, 0, 1), progressBar.transform.localScale.y, progressBar.transform.localScale.z);
		if(cur_cooldown >= max_cooldown){
			currentState=TurnState.ADDTOLIST;
		}
	}

	private IEnumerator TimeForAction(){
		if(actionStarted){
			yield break;
		}
		actionStarted = true;
		
		//Animate ennemy
		Vector3 enemyPos = new Vector3(EnnemyToAttack.transform.position.x +1.5f, EnnemyToAttack.transform.position.y, EnnemyToAttack.transform.position.z);
		while(MoveTowardEnnemy(enemyPos)){yield return null;}
		//wait
		yield return new WaitForSeconds (0.5f);
		
		//do damage
		
		
		//Animate back to position
		Vector3 firstPos = startPosition;
		while(MoveTowardStart(firstPos)){yield return null;}
		
		//remove performer from bsm list
		BSM.performList.RemoveAt (0);
		
		//reset BSM-> wait
		BSM.battleState = BattleStateMachine.PerformAction.WAIT;
		//End the coroutine
		actionStarted = false;
		//reset ennem
		cur_cooldown = 0f;
		currentState = TurnState.PROCESSING;
	}
	
	private bool MoveTowardEnnemy(Vector3 target){
		return target != (transform.position = Vector3.MoveTowards (transform.position, target, animSpeed * Time.deltaTime));
	}
	
	private bool MoveTowardStart(Vector3 target){
		return target != (transform.position = Vector3.MoveTowards (transform.position, target, animSpeed * Time.deltaTime));
	}
}

﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class HandleTurns {

	public string attacker; //Name of the attacker
	public string Type;
	public GameObject attackerGO; //GameObject of the attacker
	public GameObject attackerTarget; //Going to be attacked

}
